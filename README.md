# Andromeda

Ansible tooling to quickly recreate a standardized system and environment.

## Setup

1. Install ansible.
2. Apply the andromeda repository via ansible.

In addition to setting up the environment, this will create an 
andromeda user to periodically self update.

``` 
# Install ansible
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible

# Run ansible
sudo ansible-pull -U https://gitlab.com/jasonlttl/andromeda.git

```



